import os
import sys
from flask import (
    Flask,
    render_template,
    request,
    make_response,
    session,
    # jsonifyFlask,
    copy_current_request_context,
)
from flask_socketio import SocketIO, join_room, leave_room, send
import redis
import pickle
from threading import Lock
from flask_socketio import (
    SocketIO,
    emit,
    join_room,
    leave_room,
    close_room,
    rooms,
    disconnect,
)

from dominos.game.dominos_main import DominosGame, DominosRound, dominos_full_set
from dominos.rendering.dominos_rendering import (
    render_board_image,
    render_player_hand_image,
)


app = Flask(__name__, template_folder="templates")
app.secret_key = "BAD_SECRET_KEY"
db = redis.Redis(host="redis", port=6379)

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

REDIS_TTL_S = 60 * 10 if os.environ.get("FLASK_DEV", False) else 60 * 60 * 12
GAME_NAMESPACE = "game/"


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        socketio.sleep(10)
        count += 1
        socketio.emit("my_response", {"data": "Server generated event", "count": count})


# TODO: Would be best to have a registration page where a new game is created, or the player joins a game.
# When creating a new game you can specify how many computers.
# More advanced would be to specify which team the computers/players should join.


@app.route("/", methods=["POST", "GET"])
def login():
    if request.method == "GET":
        # Player registation page
        return render_template("game.html", async_mode=socketio.async_mode)
    # if request.method == "POST":
    #     # Player has submitted a name
    #     player_input = request.form
    #     session["dominos_round"] = []
    #     session["game"] = []

    #     # Board is set up and dominos are dealt
    #     game = DominosGame(
    #         player_names=["player1", "player2", "player3", player_input["player"]]
    #     )
    #     dominos_round = DominosRound(game)
    #     dominos_round.deal_dominos()

    #     # First play has no choice, has to be [6,6]
    #     dominos_round.play(print_output=False)

    #     while (dominos_round.active) & (
    #         dominos_round.players[dominos_round.current_player_idx].name
    #         != player_input["player"]
    #     ):
    #         dominos_round.play(print_output=False)

    #     # To be used when multiplayer:
    #     # if 'number_of_players' not in session:
    #     #     session['number_of_players'] = 0
    #     # elif session['number_of_players'] >= 4:
    #     #     return "Sorry, game is full.."
    #     # else:
    #     #     session['number_of_players'] += 1

    #     players_hand = dominos_round.players[dominos_round.current_player_idx].hand
    #     session["dominos_round"] = dominos_round.serialise()
    #     session["game"] = game.serialise()
    #     board_pic = render_board_image(dominos_round)
    #     player_hand_pic = render_player_hand_image(players_hand)
    #     return render_template(
    #         "board.html",
    #         player_name=player_input["player"],
    #         user_image=board_pic,
    #         player_hand_pic=player_hand_pic,
    #     )


# @app.route("/play", methods=["POST", "GET"])
# def play():
#     # if 'player_name' in session:
#     #     player_name = session['player_name']
#     #     return 'Logged in as ' + player_name + '<br>' + "<b><a href = '/logout'>click here to log out</a></b>"
#     # return "You are not logged in <br><a href = '/login'>" + "click here to log in</a>"
#     out = 0
#     # if request.method == 'GET':
#     #     board_image = "test.png"
#     #     player_input = request.form
#     #     return render_template('board.html', player_name = session['player_name'], user_image=board_image)
#     if request.method == "POST":
#         # Either it's players turn, or they are told they need to wait
#         # if players_turn:
#         #   player_plays()
#         player_input = request.form
#         # TODO: When a player selects a domino, they should also specify which end it needs to go on.
#         # If you have a [2,1] domino, and the two ends of the table are 2 and 1, then it matters which
#         # end it goes on.
#         dominos_round = DominosRound(serialisation=session["dominos_round"])
#         game = DominosGame(serialisation=session["game"])
#         # Here the players move is made
#         # TODO: Check the ordering of the players is not messed up when serialising/deserialising

#         if player_input["domino_idx"] in ["1", "2", "3", "4", "5", "6", "7"]:
#             domino_idx = int(player_input["domino_idx"])
#         else:
#             domino_idx = None
#             # TODO: If no domino has been selected, but the player can go, then they should be prompted again.

#         dominos_round.play(print_output=False, input_domino_idx=domino_idx)

#         # Then computers (or other players go:)
#         while (dominos_round.active) & (
#             dominos_round.players[dominos_round.current_player_idx].name
#             != player_input["player"]
#         ):
#             dominos_round.play(print_output=False)

#         # TODO: Show other players hands somehow
#         # TODO: If selected domino is not legal the same board/hand pic should be returned with a notice saying illegal move.
#         # TODO: If no domino is selected then no move should be made?
#         players_hand = dominos_round.players[dominos_round.current_player_idx].hand
#         session["dominos_round"] = []
#         session["game"] = []
#         session["dominos_round"] = dominos_round.serialise()
#         session["game"] = game.serialise()
#         board_pic = render_board_image(dominos_round)
#         player_hand_pic = render_player_hand_image(players_hand)

#         # TODO: Play out the whole game.
#         # TODO: Have feedback to user if their move is illegal, or if the game is over.
#         # TODO: Need a 'skip turn' button - should only be allowed if you can't go though. Or some kind of notification that you have to skip, then whatever is input is ignored anyway.
#         # TODO: When round is finished you should click for next round. Then when game finishes you click for new game.
#         # TODO: Can total scores be printed on screen? To keep a tally.

#         # if ((session['game'].team_1_score > 100) or (session['game'].team_2_score > 100)):
#         #     return render_template('board.html', player_name = player_input['player'], user_image=board_pic)
#         # else:
#         if dominos_round.active:
#             return render_template(
#                 "board.html",
#                 player_name=player_input["player"],
#                 user_image=board_pic,
#                 player_hand_pic=player_hand_pic,
#             )
#         else:
#             # TODO: These scores should be calculated in the class
#             team_1_round_score = (
#                 dominos_round.players[0].score + dominos_round.players[2].score
#             )
#             team_2_round_score = (
#                 dominos_round.players[1].score + dominos_round.players[3].score
#             )
#             if team_1_round_score > team_2_round_score:
#                 # TODO: By default the player is players[3]. This would be different in multiplayer
#                 outcome = "won :)"
#             else:
#                 outcome = "lost :("
#             return render_template(
#                 "board_finished.html",
#                 player_name=player_input["player"],
#                 user_image=board_pic,
#                 player_hand_pic=player_hand_pic,
#                 outcome=outcome,
#             )


socketio = SocketIO(app, logger=True, engineio_logger=True)


@socketio.event
def start_game(player_input):
    # Player has submitted a name
    session["dominos_round"] = []
    session["game"] = []

    # Board is set up and dominos are dealt
    game = DominosGame(
        player_names=["player1", "player2", "player3", player_input["player"]]
    )
    dominos_round = DominosRound(game)
    dominos_round.deal_dominos()

    # First play has no choice, has to be [6,6]
    dominos_round.play(print_output=False)

    while (dominos_round.active) & (
        dominos_round.players[dominos_round.current_player_idx].name
        != player_input["player"]
    ):
        dominos_round.play(print_output=False)

    # To be used when multiplayer:
    # if 'number_of_players' not in session:
    #     session['number_of_players'] = 0
    # elif session['number_of_players'] >= 4:
    #     return "Sorry, game is full.."
    # else:
    #     session['number_of_players'] += 1

    players_hand = dominos_round.players[dominos_round.current_player_idx].hand
    session["dominos_round"] = dominos_round.serialise()
    session["game"] = game.serialise()
    board_pic = render_board_image(dominos_round)
    player_hand_pic = render_player_hand_image(players_hand)
    return render_template(
        "board.html",
        player_name=player_input["player"],
        user_image=board_pic,
        player_hand_pic=player_hand_pic,
    )


######


@socketio.event
def my_event(message):
    session["receive_count"] = session.get("receive_count", 0) + 1
    emit("my_response", {"data": message["data"], "count": session["receive_count"]})


@socketio.event
def my_broadcast_event(message):
    session["receive_count"] = session.get("receive_count", 0) + 1
    emit(
        "my_response",
        {"data": message["data"], "count": session["receive_count"]},
        broadcast=True,
    )


@socketio.event
def join(message):
    join_room(message["room"])
    session["receive_count"] = session.get("receive_count", 0) + 1

    emit(
        "player_joining",
        {
            "data": "In rooms: " + ", ".join(rooms()),
            "player_name": message["player_name"],
        },
        to=message["room"],
    )


@socketio.event
def player_joined(game_data):
    emit("game", game_data)


@socketio.event
def leave(message):
    leave_room(message["room"])
    session["receive_count"] = session.get("receive_count", 0) + 1
    emit(
        "my_response",
        {"data": "In rooms: " + ", ".join(rooms()), "count": session["receive_count"]},
    )


@socketio.on("close_room")
def on_close_room(message):
    session["receive_count"] = session.get("receive_count", 0) + 1
    emit(
        "my_response",
        {
            "data": "Room " + message["room"] + " is closing.",
            "count": session["receive_count"],
        },
        to=message["room"],
    )
    close_room(message["room"])


@socketio.event
def my_room_event(message):
    session["receive_count"] = session.get("receive_count", 0) + 1
    emit(
        "my_response",
        {"data": message["data"], "count": session["receive_count"]},
        to=message["room"],
    )


@socketio.event
def disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()

    session["receive_count"] = session.get("receive_count", 0) + 1
    # for this emit we use a callback function
    # when the callback function is invoked we know that the message has been
    # received and it is safe to disconnect
    emit(
        "my_response",
        {"data": "Disconnected!", "count": session["receive_count"]},
        callback=can_disconnect,
    )


@socketio.event
def my_ping():
    emit("my_pong")


@socketio.event
def connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(background_thread)
    emit("new_player_connected", {"id": request.sid}, broadcast=True)
    emit("connected", {"id": request.sid})


@socketio.event
def players_present(players_present):
    emit("all_players_present", {"players_present": players_present}, broadcast=True)


@socketio.on("disconnect")
def test_disconnect():
    print("Client disconnected", request.sid)


def get_game(room, prefix=True):
    room = GAME_NAMESPACE + room if prefix else room
    gm = db.get(room)
    if gm:
        return pickle.loads(gm)
    else:
        emit("error", {"error": "Unable to join, room does not exist."})
        return None


def save_game(game):
    db.setex(GAME_NAMESPACE + game.game_id, REDIS_TTL_S, pickle.dumps(game))


@socketio.on("start_game")
def on_create(data):
    """Create a game lobby"""

    game_id = "1"

    # add current user to players list
    gm = {}
    gm["players"] = {}
    gm["players"].update({data["player"]: request.sid})

    room_players = {request.sid: game_id}

    # write room to redis
    join_room(game_id)
    # Save game:
    db.setex("ROOM_PLAYERS", REDIS_TTL_S, pickle.dumps(room_players))
    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    emit("join_room", {"room": game_id})


@socketio.on("join")
def on_join(data):
    """Join a game lobby"""
    # TODO: Should be able to click 'start game' at any time and missing players are then comuter players
    # room_id should be game id and should be dynamic
    # The board should be same for all players in a room, but player hands should be specific to them.

    game_id = data["room"]

    # add current user to players list
    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)

    gm["players"].update({data["player_name"]: request.sid})

    # write room to redis
    join_room(game_id)
    # Save game:
    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    # if len(gm["players"]) == 1:
    # Board is set up and dominos are dealt
    player_names = list(gm["players"].keys())
    game = DominosGame(player_names=["A", "B", "C", "D"])
    dominos_round = DominosRound(game)
    gm.update({"game": game, "round": dominos_round})

    room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    room_players.update({request.sid: game_id})

    db.setex("ROOM_PLAYERS", REDIS_TTL_S, pickle.dumps(room_players))
    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    dominos_round.deal_dominos()

    # First play has no choice, has to be [6,6]
    dominos_round.play(print_output=False)

    players_hand = dominos_round.players[dominos_round.current_player_idx].hand
    board_pic = render_board_image(dominos_round)
    player_hand_pic = render_player_hand_image(players_hand)
    emit(
        "update_board",
        {"board_pic": board_pic[0]},
        to=game_id,
    )
    emit(
        "update_player_hand",
        {"player_hand_pic": player_hand_pic[0]},
    )

    emit("join_room", {"room": game_id})


@socketio.on("play_domino")
def on_play_domino(data):

    # add current user to players list
    room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    game_id = room_players[request.sid]

    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)

    domino_to_play = data["domino_to_play"]
    # TODO: When a player selects a domino, they should also specify which end it needs to go on.
    # If you have a [2,1] domino, and the two ends of the table are 2 and 1, then it matters which
    # end it goes on.
    dominos_round = gm["round"]
    game = gm["game"]
    # Here the players move is made
    # TODO: Check the ordering of the players is not messed up when serialising/deserialising

    if domino_to_play in ["1", "2", "3", "4", "5", "6", "7"]:
        domino_idx = int(domino_to_play)
    else:
        domino_idx = None
        # TODO: If no domino has been selected, but the player can go, then they should be prompted again.

    dominos_round.play(print_output=False, input_domino_idx=domino_idx)

    # Then computers (or other players go:)
    while (dominos_round.active) & (
        dominos_round.players[dominos_round.current_player_idx].name
        != player_input["player"]
    ):
        dominos_round.play(print_output=False)

    # TODO: Show other players hands somehow
    # TODO: If selected domino is not legal the same board/hand pic should be returned with a notice saying illegal move.
    # TODO: If no domino is selected then no move should be made?
    players_hand = dominos_round.players[dominos_round.current_player_idx].hand

    ##################
    # write room to redis
    join_room(game_id)
    # Save game:
    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    # if len(gm["players"]) == 1:
    # Board is set up and dominos are dealt
    player_names = list(gm["players"].keys())
    game = DominosGame(player_names=["A", "B", "C", "D"])
    dominos_round = DominosRound(game)
    gm.update({"game": game, "round": dominos_round})

    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    dominos_round.deal_dominos()

    # First play has no choice, has to be [6,6]
    dominos_round.play(print_output=False)

    players_hand = dominos_round.players[dominos_round.current_player_idx].hand
    board_pic = render_board_image(dominos_round)
    player_hand_pic = render_player_hand_image(players_hand)
    emit(
        "update_board",
        {"board_pic": board_pic[0]},
        to=game_id,
    )
    emit(
        "update_player_hand",
        {"player_hand_pic": player_hand_pic[0]},
    )

    emit("join_room", {"room": game_id})


@socketio.on("create")
def on_create(data):
    """Create a game lobby"""
    # username = data['username']
    # create the game
    # handle custom wordbanks
    game = []
    # TODO: Create game should send you to the page where you enter name
    # Join game should also then send you to the name entry page.
    # then Waiting for other players to join page or?

    db.setex(GAME_NAMESPACE, REDIS_TTL_S, pickle.dumps(data))
    if data["dictionaryOptions"]["useCustom"]:
        gm = game.Game(
            size=data["size"],
            teams=data["teams"],
            wordbank=data["dictionaryOptions"]["customWordbank"],
        )
    # dict mixer
    elif data["dictionaryOptions"]["mix"]:
        gm = game.Game(
            size=data["size"],
            teams=data["teams"],
            mix=data["dictionaryOptions"]["mixPercentages"],
        )

    # handle standard single dictionary
    else:
        gm = game.Game(
            size=data["size"],
            teams=data["teams"],
            dictionary=data["dictionaryOptions"]["dictionaries"],
        )

    # add current user to players list
    gm.players.add(request.sid, data.get("username", None))

    # check if ID exists
    while get_game(gm.game_id) is not None:
        gm.regenerate_id()

    # write room to redis
    join_room(gm.game_id)
    save_game(gm)
    emit("join_room", {"room": gm.game_id})


if __name__ == "__main__":

    socketio.run(app)
