INTRODUCE a diagram
Write down what you KNOW
Be clear on what you WANT
Break up what you WANT into manageable pieces

What are the main objects?:

- Deck of cards (Card count = 52)

- Communal cards (three parts)
    - Flop - visible after first round of betting
    - Turn - visible after second round of betting
    - River - visible after third round of betting

- Player hands (Card count = 2 * Number of players) 
    |
    > Players know their own hands but others are hidden

What are the actions?:

- The Deck should be shuffled, then cards are dealt, then the other cards in the deck are no longer used.

- So class could be 'poker game' which has:
    - Deck
    - Players

    -Rounds
        - Player hands
        - Flop, Turn, River