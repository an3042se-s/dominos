import random
from src.frenchdeck import FrenchDeck


cards = FrenchDeck()


class PokerGame():
    def __init__(self, player_names):
        self.players = [Player(player_names[0]), Player(player_names[1])]
        self.scores = []
        self.team_1_round_score = 0
        self.team_2_round_score = 0
        self.team_1_score = 0
        self.team_2_score = 0
        self.update_scores_chart()

    def update_scores_chart(self):
        self.round_score_chart = " Team 1 | Team 2  \n-----------------\n" + "  " + str(self.team_1_round_score).zfill(3) + "   |  " + str(self.team_2_round_score).zfill(3) + "\n"
        self.total_score_chart = " Team 1 | Team 2  \n-----------------\n" + "  " + str(self.team_1_score).zfill(3) + "   |  " + str(self.team_2_score).zfill(3) + "\n"

    def print_round_summary(self):
        print("=============\nRound Finished\n=============")
        if self.team_1_round_score < self.team_2_round_score:
            print('Team 1 wins this round')
        elif self.team_2_round_score < self.team_1_round_score:
            print('Team 2 wins this round')
        self.update_scores_chart()
        print(self.round_score_chart)
        if self.team_1_score < self.team_2_score:
            print('Team 1 leads')
        elif self.team_2_score < self.team_1_score:
            print('Team 2 leads')
        print(self.total_score_chart)

    def print_final_scores(self):
        print("=============\nGame Finished\n=============")
        print("*Final Scores:\n")
        if self.team_1_score < self.team_2_score:
            print('Team 1 wins!')
        elif self.team_2_score < self.team_1_score:
            print('Team 2 wins!')
        self.update_scores_chart()
        print(self.total_score_chart)


class PokerRound():
    def __init__(self, poker_game):
        self.poker_game = poker_game
        self.table = []
        self.active = True
        self.players = poker_game.players
        self.current_player = []
        
    def deal_cards(self):
        cards.shuffle()
        self.players[0].hand = cards[0:2]
        self.players[1].hand = cards[2:4]

    def play(self):
        self = self.current_player.play(self)
        self.next_player()

    def next_player(self):
        # TODO: make this more pythonic
        self.current_player = self.players[([player.name for player in self.players].index(self.current_player.name) + 1) % 4]
    
    def print_table(self):
        print("Table:\n------")
        print(self.table)
        print("\nHands:\n------")
        for player in self.players:
            print(player.hand)

    def get_scores(self, dominos_game):
        for player in self.players:
            player_score = 0
            for list in player.hand:
                for item in list:
                    player_score = player_score + item
            player.score = player_score
        dominos_game.team_1_round_score = self.players[0].score + self.players[2].score
        dominos_game.team_2_round_score = self.players[1].score + self.players[3].score
        dominos_game.team_1_score = dominos_game.team_1_score + dominos_game.team_1_round_score
        dominos_game.team_2_score = dominos_game.team_2_score + dominos_game.team_2_round_score


class Player():
    def __init__(self, name):
        self.name = name
        self.score = 0
        self.hand = []
    def check_options():
        print('checking play options')
    
    def play(self, round):
        ends = [round.table[0][0], round.table[-1][-1]]

        # Check available options:
        ##########################
        options = []
        for a_domino in self.hand:
            if ends[0] in a_domino:
                options = options + [[a_domino] + [[a_domino.index(ends[0]), 'left']]]
            elif ends[1] in a_domino:
                    options = options + [[a_domino] + [[a_domino.index(ends[1]), 'right']]]

        if options:
            round.player_could_not_go_count = 0

            # Here you can add player choice from available options:
            ##################################
            random.shuffle(options)
            selected_domino = options[0]
            ##################################
            
            selected_domino_idx = round.current_player.hand.index(selected_domino[0])
            domino_idx = selected_domino[1][0]
            table_idx = selected_domino[1][1]
            play = round.current_player.hand.pop(selected_domino_idx)

            if table_idx == 'left':
                if domino_idx == 0:
                    play.reverse()
                table = [play] + round.table
            elif table_idx == 'right':
                if domino_idx == 1:
                    play.reverse()
                table = round.table + [play]
            
            print(round.current_player.name + " has played" + "\n")
            print("Table:\n------")
            print(round.table)
            print("\nHands:\n------")
            for player in round.players:
                print(player.hand)
            print("\n")
                
        else:
            print(round.current_player.name + " could not go\n*********************")
            round.player_could_not_go_count = round.player_could_not_go_count + 1
        
        return round


if __name__=="__main__":

    # Initiate game:
    ################
    game = DominosGame(player_names=['player1', 'player2', 'player3', 'player4'])

    # Start playing:
    ################
    while True:
        # New round:
        dominos_round = DominosRound(game)
        dominos_round.deal_dominos()

        while dominos_round.active:
            dominos_round.play()

        game.print_round_summary()

        if ((game.team_1_score > 100) or (game.team_2_score > 100)):
            break

    game.print_final_scores()
