import collections
import random

Card = collections.namedtuple('Card', ['rank', 'suit'])

class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    suits = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits
                                        for rank in self.ranks]
        self._dealt_cards = []

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]

    def __add__(self, other):
        combined_cards = self._cards + other._cards
        return combined_cards

    def deal(self, N=1):
        cards = []
        for n in range(N):
            card = self._cards.pop()
            self._dealt_cards = self._dealt_cards + [card]
            cards = cards + [card]
        return cards
    
    def shuffle(self):
        random.shuffle(self._cards)
