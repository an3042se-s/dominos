FROM python:3.8

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

COPY . .
RUN apt-get update && apt-get install sudo

WORKDIR /server
CMD ["python", "app.py"]
