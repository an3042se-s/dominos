import os
import sys
import pandas as pd
from flask import (
    Flask,
    render_template,
    request,
    make_response,
    session,
    copy_current_request_context,
    jsonify,
)
from flask_socketio import SocketIO, join_room, leave_room, send
import redis
import pickle
from threading import Lock
from flask_socketio import (
    SocketIO,
    emit,
    join_room,
    leave_room,
    close_room,
    rooms,
    disconnect,
)
import random
import string

from dominos.game.dominos_main import DominosGame, DominosRound, dominos_full_set
from dominos.rendering.dominos_rendering import (
    render_board_image,
    render_player_hand_image,
)


# TODO:
# Users need to see state of the game:
#   - Whose turn is it?
#   - What are the scores?
# There is a glitch that needs to be fixed where the train turns back on itself.
# Bug related to dropzones - they are persisting. Also the orientation is not always correct.
# Players should be able to choose/swap teams in the main game ui.
# AI - implement MCTS algorithm
# How will it be shared on Heroku?
# Need to have the full system running using docker-compose file.
# Ngnix? Does the wsgi need to be a 'deployment' version?
# Overall tidying of scripts
#   - Review all scripts and remove code/files that are not needed
#   - Look for opportunities to refactor/simplify/improve the code.
#       - Is it easy to read?
#       - Is there a simpler way to do any of the code?

app = Flask(__name__, template_folder="templates")
app.secret_key = "BAD_SECRET_KEY"

db = redis.from_url(os.environ.get("REDIS_URL"))
# db = redis.Redis(host="redis", port=6379)

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None
socketio = SocketIO(
    app,
    logger=True,
    engineio_logger=True,
    async_mode=async_mode,
    cors_allowed_origins=[
        "http://localhost:8080",
        "http://localhost:5000",
        "http://localhost:8000/",
        "//dominos:5000",
    ],
    methods=["GET", "POST"],
)
thread = None
thread_lock = Lock()

REDIS_TTL_S = 60 * 10 if os.environ.get("FLASK_DEV", False) else 60 * 60 * 12
GAME_NAMESPACE = "game/"

# TODO: Would be best to have a registration page where a new game is created, or the player joins a game.
# When creating a new game you can specify how many computers.
# More advanced would be to specify which team the computers/players should join.

# TODO: Show other players hands somehow - the backs - players should know how many dominos the other players have.

# TODO: When a player selects a domino, they should also specify which end it needs to go on.
# If you have a [2,1] domino, and the two ends of the table are 2 and 1, then it matters which
# end it goes on.

# TODO: There could be a notification saying "It's your turn", "It's player X's turn"

# TODO:
# - Allow players to specify which teams they want to join
# - Have setup page which leads into the game page?
# - See number of dominos held, and location, for each of the other players.
# - Game ends and result should be shown. Then offer to play again. Also scores for game/round
# - Option to leave game? - other players then need to be notified.
# - better AI
# - better front end

# TODO: What happens when a player leaves the game?


@socketio.on("test_ping")
def test_ping(data):
    emit("testPong", {})


# sanity check route
@app.route("/ping", methods=["GET"])
def ping_pong():
    return jsonify("pong!")


@app.route("/", methods=["POST", "GET"])
def login():
    if request.method == "GET":
        # Player registation page
        return render_template("game.html", async_mode=socketio.async_mode)


def get_game(room, prefix=True):
    room = GAME_NAMESPACE + room if prefix else room
    gm = db.get(room)
    if gm:
        return pickle.loads(gm)
    else:
        emit("error", {"error": "Unable to join, room does not exist."})
        return None


def save_game(game):
    db.setex(GAME_NAMESPACE + game.game_id, REDIS_TTL_S, pickle.dumps(game))


@socketio.on("reset_table")
def on_reset_table(data):
    print(data)
    emit("resetTable")


@socketio.on("start_game")
def on_start_game(data):
    """Create a game lobby"""

    # id_length = 5
    # game_id = "".join(
    #     random.SystemRandom().choice(string.ascii_uppercase) for _ in range(id_length)
    # )

    # # Add current user to players list:
    # gm = {}
    # gm["players"] = {}
    # gm["players"].update({data["player_name"]: request.sid})
    # gm["game_id"] = game_id
    # gm["number_of_computer_players"] = int(data["number_of_computer_players"])

    # # Join game room:
    # join_room(game_id)

    # TODO: The ROOM_PLAYERS should be initialised if it does not exist.
    # If a new game is started by another group it shouldn't overwrite
    # so ROOM_PLAYERS needs to be loaded first.
    # TODO: Here player' is used, but below 'player_name' is used.
    # if db.get("ROOM_PLAYERS") is None:
    #     room_players = {}
    # else:
    room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    # room_players.update(
    #     {request.sid: {"name": data["player_name"], "game_id": game_id}}
    # )
    game_id = room_players[request.sid]["game_id"]

    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)
    else:
        raise ValueError("No room with that ID")

    # TODO: Computer player names need to be identifiable - e.g. the name of the actual players is the sid, whereas for computers it can be text.
    gm["number_of_computer_players"] = 4 - len(gm["players"])
    if gm["number_of_computer_players"] > 0:
        for n in range(gm["number_of_computer_players"]):
            gm["players"].update(
                {"computer_" + str(n): "computer_" + str(n),}
            )
        start_game(gm)
    # Save game:
    db.setex("ROOM_PLAYERS", REDIS_TTL_S, pickle.dumps(room_players))
    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    emit("join_room", {"game_room_message": "You are in room: " + str(game_id)})


def start_game(gm):
    player_names = list(gm["players"].keys())
    # TODO: the player names should be the request.sid. Then maybe have a dictionary with the input names.

    game = DominosGame(player_names=player_names)
    dominos_round = DominosRound(game)

    # Here the dominos game begins:
    dominos_round.deal_dominos()
    # First play has no choice, has to be [6,6]
    current_player_name = dominos_round.players[dominos_round.current_player_idx].name
    if current_player_name.__contains__("computer"):
        # TODO: need a better way of identifying if it is computer
        while (dominos_round.active) & (
            dominos_round.players[dominos_round.current_player_idx].name.__contains__(
                "computer"
            )
        ):
            dominos_round.play(print_output=False)
        gm["drop_zone_coordinates"] = dominos_round.drop_zone_coordinates
        gm["zoneA_accepts"] = dominos_round.table[0][0]
        gm["zoneB_accepts"] = dominos_round.table[-1][1]
    else:
        gm["drop_zone_coordinates"] = [[1650, 880]]
        gm["zoneA_accepts"] = 6
        gm["zoneB_accepts"] = 6

    gm.update({"game": game, "round": dominos_round})
    db.setex(GAME_NAMESPACE + gm["game_id"], REDIS_TTL_S, pickle.dumps(gm))

    # # Create board rendering:
    # board_pic = render_board_image(dominos_round)
    # scores = game.total_score_chart

    # emit(
    #     "update_game",
    #     {
    #         "table": gm["round"].table,
    #         "domino_coordinates": gm["round"].domino_coordinates,
    #         "domino_orientation": gm["round"].domino_orientation,
    #         "drop_zone_coordinates": drop_zone_coordinates,
    #         "zoneA_accepts": zoneA_accepts,
    #         "zoneB_accepts": zoneB_accepts,
    #         "game_id": gm["game_id"],
    #     },
    #     to=gm["game_id"],
    # )
    emit(
        "update_game", {"game_id": gm["game_id"]}, to=gm["game_id"],
    )
    # emit(
    #     "update_scores",
    #     {"scores": scores},
    #     to=gm["game_id"],
    # )


@socketio.on("join")
def on_join(data):
    """Join a game lobby"""

    # TODO: If no player name is supplied then name should be provided (e.g. user1234)
    game_id = data["room"]

    # add current user to players list
    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)
        gm["players"].update({data["player_name"]: request.sid})
    else:
        gm = {}
        gm["players"] = {}
        gm["players"].update({data["player_name"]: request.sid})
        gm["game_id"] = game_id

    # Join game room:
    join_room(game_id)

    # TODO: The ROOM_PLAYERS should be initialised if it does not exist.
    # If a new game is started by another group it shouldn't overwrite
    # so ROOM_PLAYERS needs to be loaded first.
    # TODO: Here player' is used, but below 'player_name' is used.
    if db.get("ROOM_PLAYERS") is None:
        room_players = {}
    else:
        room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    room_players.update(
        {request.sid: {"name": data["player_name"], "game_id": game_id}}
    )

    # # TODO: Computer player names need to be identifiable - e.g. the name of the actual players is the sid, whereas for computers it can be text.
    # if gm["number_of_computer_players"] == 3:
    #     gm["players"].update(
    #         {
    #             "computer_1": "computer_1",
    #             "computer_2": "computer_2",
    #             "computer_3": "computer_3",
    #         }
    #     )
    #     # start_game(gm)
    # Save game:
    db.setex("ROOM_PLAYERS", REDIS_TTL_S, pickle.dumps(room_players))
    db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    # # write room to redis
    # join_room(game_id)

    # # Save game:
    # db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

    # # Update players in room
    # room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    # room_players.update(
    #     {request.sid: {"name": data["player_name"], "game_id": game_id}}
    # )
    # db.setex("ROOM_PLAYERS", REDIS_TTL_S, pickle.dumps(room_players))

    # emit("join_room", {"game_room_message": "You are in room: " + str(game_id)})

    # if gm["number_of_computer_players"] + len(gm["players"]) > 3:
    #     computer_players = dict(
    #         [
    #             ("computer_" + str(idx), "computer_" + str(idx))
    #             for idx in range(gm["number_of_computer_players"])
    #         ]
    #     )
    #     gm["players"].update(computer_players)
    # start_game(gm)


@socketio.on("game_updated")
def on_game_updated(data):

    room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    game_id = room_players[request.sid]["game_id"]

    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)
    else:
        raise ValueError("No room with that ID")

    dominos_round = gm["round"]
    game = gm["game"]
    player_name = room_players[request.sid]["name"]
    # Whats a better way to do this?:
    player_idx = [dominos_round.players[idx].name for idx in [0, 1, 2, 3]].index(
        player_name
    )
    player_hands = []
    for idx in range(4):
        player_hands = player_hands + [dominos_round.players[player_idx].hand]
        player_idx = (player_idx + 1) % 4

    # player_hand_pic = render_player_hand_image(players_hand)

    emit(
        "update_player_hand",
        {
            "table": gm["round"].table,
            "domino_coordinates": gm["round"].domino_coordinates,
            "domino_orientation": gm["round"].domino_orientation,
            "drop_zone_coordinates": gm["drop_zone_coordinates"],
            "zoneA_accepts": gm["zoneA_accepts"],
            "zoneB_accepts": gm["zoneB_accepts"],
            "game_id": gm["game_id"],
            "player": player_hands[0],
            "player1": player_hands[1],
            "player2": player_hands[2],
            "player3": player_hands[3],
            "game_id": gm["game_id"],
            "team_1_score": gm["round"].dominos_game.team_1_score,
            "team_2_score": gm["round"].dominos_game.team_2_score,
        },
    )


@socketio.on("play_domino")
def on_play_domino(data):

    # Check if it's the players turn, if not then break and display message saying whose turn it is.

    room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    game_id = room_players[request.sid]["game_id"]
    player_name = room_players[request.sid]["name"]
    domino_to_play = data["domino_to_play"]
    end_to_play = data["end_to_play"]

    # Load current state of game:
    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)

    dominos_round = gm["round"]
    game = gm["game"]

    # Selected player plays:
    ########################
    player_idx = [dominos_round.players[idx].name for idx in [0, 1, 2, 3]].index(
        player_name
    )
    if domino_to_play:
        domino_idx = dominos_round.players[player_idx].hand.index(domino_to_play)
    else:
        domino_idx = None

    if not player_idx == dominos_round.current_player_idx:
        emit(
            "message_to_player", {"message": "It's not your turn."},
        )
    else:
        if not player_name.__contains__("computer"):
            dominos_round.play(
                print_output=False, input_domino_idx=domino_idx, end_to_play=end_to_play
            )

        # If computers turn they go:
        while (dominos_round.active) & (
            dominos_round.players[dominos_round.current_player_idx].name.__contains__(
                "computer"
            )
        ):
            dominos_round.play(print_output=False)

        # If next player has run out of dominos then continue to next player:
        while not dominos_round.players[dominos_round.current_player_idx].hand:
            dominos_round.next_player()

        if not dominos_round.active:
            game.update_scores_chart()
            if (game.team_1_score > 100) or (game.team_2_score > 100):
                scores = game.total_score_chart
            else:
                scores = game.total_score_chart
            emit(
                "update_scores", {"scores": scores}, to=game_id,
            )

        gm["drop_zone_coordinates"] = gm["round"].drop_zone_coordinates
        gm["zoneA_accepts"] = dominos_round.table[0][0]
        gm["zoneB_accepts"] = dominos_round.table[-1][1]

        # Update the game state:
        ########################
        gm.update({"game": game, "round": dominos_round})
        db.setex(GAME_NAMESPACE + game_id, REDIS_TTL_S, pickle.dumps(gm))

        emit(
            "update_game", {"game_id": game_id}, to=game_id,
        )


@socketio.on("continue_play")
def on_continue_play(data):

    room_players = pickle.loads(db.get("ROOM_PLAYERS"))
    game_id = room_players[request.sid]["game_id"]

    dat = db.get(GAME_NAMESPACE + game_id)
    if dat:
        gm = pickle.loads(dat)

    dominos_round = gm["round"]
    game = gm["game"]

    if not dominos_round.active:
        if (game.team_1_score > 100) or (game.team_2_score > 100):
            game = DominosGame(player_names=list(gm["players"].keys()))
        dominos_round = DominosRound(dominos_game=game)
        dominos_round.deal_dominos()

        # First play has no choice, has to be [6,6]
        current_player_name = dominos_round.players[
            dominos_round.current_player_idx
        ].name
        if current_player_name.__contains__("computer"):
            while (dominos_round.active) & (
                dominos_round.players[
                    dominos_round.current_player_idx
                ].name.__contains__("computer")
            ):
                dominos_round.play(print_output=False)
            gm["drop_zone_coordinates"] = dominos_round.drop_zone_coordinates
            gm["zoneA_accepts"] = dominos_round.table[0][0]
            gm["zoneB_accepts"] = dominos_round.table[-1][1]
        else:
            gm["drop_zone_coordinates"] = [[1650, 880]]
            gm["zoneA_accepts"] = 6
            gm["zoneB_accepts"] = 6

        gm.update({"game": game, "round": dominos_round})
        db.setex(GAME_NAMESPACE + gm["game_id"], REDIS_TTL_S, pickle.dumps(gm))

        emit(
            "update_game", {"game_id": game_id}, to=game_id,
        )
    else:
        emit(
            "message_to_player", {"message": "Current game still in progress."},
        )


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    socketio.run(app, host="0.0.0.0", port=port)
