from utils.relative_directories import ROOT_DIR
from dominos.rendering.text_converter import text_image


def render_board_image(dominos_round):
    # Render board image:
    dominos_pic = []
    for domino in dominos_round.table:
        dominos_pic += [get_domino_horizontal(domino)]
    rows = []
    for idx in range(7):
        fill = "".join([dominos_pic[ii][idx] for ii in range(len(dominos_round.table))])
        rows = rows + [fill]
    board = (
        "\n"
        + rows[0]
        + "\n"
        + rows[1]
        + "\n"
        + rows[2]
        + "\n"
        + rows[3]
        + "\n"
        + rows[4]
        + "\n"
        + rows[5]
        + "\n"
        + rows[6]
    )
    txt_file = ROOT_DIR + "/server/static/test_board.txt"
    png_file = ROOT_DIR + "/server/static/test_board.png"
    print(board, file=open(txt_file, "w"))
    img = text_image(txt_file)
    img.save(png_file)
    board_pic = "test_board.png"
    return (board, board_pic)


def render_player_hand_image(players_hand):
    # Render board image:
    dominos_pic = []
    for domino in players_hand:
        dominos_pic += [get_domino_horizontal(domino, force_vertical=True)]
    rows = []
    for idx in range(7):
        fill = "".join([dominos_pic[ii][idx] for ii in range(len(players_hand))])
        rows = rows + [fill]
    board = (
        "\n"
        + rows[0]
        + "\n"
        + rows[1]
        + "\n"
        + rows[2]
        + "\n"
        + rows[3]
        + "\n"
        + rows[4]
        + "\n"
        + rows[5]
        + "\n"
        + rows[6]
    )
    txt_file = ROOT_DIR + "/server/static/test_hand.txt"
    png_file = ROOT_DIR + "/server/static/test_hand.png"
    print(board, file=open(txt_file, "w"))
    img = text_image(txt_file)
    img.save(png_file)
    board_pic = "test_hand.png"
    return (board, board_pic)


def get_domino_horizontal(val, force_vertical=False):
    if (val[0] == val[1]) or force_vertical:
        domino = [
            "     ",
            "+---+",
            "| " + str(val[0]) + " |",
            "|---|",
            "| " + str(val[1]) + " |",
            "+---+",
            "     ",
        ]
    else:
        domino = [
            "         ",
            "         ",
            "+-------+",
            "| " + str(val[0]) + " | " + str(val[1]) + " |",
            "+-------+",
            "         ",
            "         ",
        ]
    return domino
