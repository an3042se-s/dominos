import random
from itertools import compress
import numpy as np
from .dominos_mcts import mcts_selection

dominos_full_set = [
    [0, 0],
    [1, 0],
    [1, 1],
    [2, 0],
    [2, 1],
    [2, 2],
    [3, 0],
    [3, 1],
    [3, 2],
    [3, 3],
    [4, 0],
    [4, 1],
    [4, 2],
    [4, 3],
    [4, 4],
    [5, 0],
    [5, 1],
    [5, 2],
    [5, 3],
    [5, 4],
    [5, 5],
    [6, 0],
    [6, 1],
    [6, 2],
    [6, 3],
    [6, 4],
    [6, 5],
    [6, 6],
]


def print_domino(domino):
    val1 = domino[0]
    val2 = domino[1]
    domino_string = (
        "\n+===+" + "\n| " + str(val1) + " |\n|---|\n| " + str(val2) + " |\n+===+"
    )
    print(domino_string)


class DominosGame:
    def __init__(self, player_names=None, serialisation=None):
        if serialisation is not None:
            self.deserialise(serialisation)
        else:
            self.players = [
                Player(player_names[0]),
                Player(player_names[1]),
                Player(player_names[2]),
                Player(player_names[3]),
            ]
            self.scores = []
            self.team_1_round_score = 0
            self.team_2_round_score = 0
            self.team_1_score = 0
            self.team_2_score = 0
            self.update_scores_chart()

    def update_scores_chart(self):
        self.round_score_chart = (
            " Team 1 | Team 2  \n-----------------\n"
            + "  "
            + str(self.team_1_round_score).zfill(3)
            + "   |  "
            + str(self.team_2_round_score).zfill(3)
            + "\n"
        )
        self.total_score_chart = (
            " Team 1 | Team 2  \n-----------------\n"
            + "  "
            + str(self.team_1_score).zfill(3)
            + "   |  "
            + str(self.team_2_score).zfill(3)
            + "\n"
        )

    def print_round_summary(self):
        print("=============\nRound Finished\n=============")
        if self.team_1_round_score < self.team_2_round_score:
            print("Team 1 wins this round")
        elif self.team_2_round_score < self.team_1_round_score:
            print("Team 2 wins this round")
        self.update_scores_chart()
        print(self.round_score_chart)
        if self.team_1_score < self.team_2_score:
            print("Team 1 leads")
        elif self.team_2_score < self.team_1_score:
            print("Team 2 leads")
        print(self.total_score_chart)

    def print_final_scores(self):
        print("=============\nGame Finished\n=============")
        print("*Final Scores:\n")
        if self.team_1_score < self.team_2_score:
            print("Team 1 wins!")
        elif self.team_2_score < self.team_1_score:
            print("Team 2 wins!")
        self.update_scores_chart()
        print(self.total_score_chart)

    def serialise(self):
        return {
            "players": [player.serialise() for player in self.players],
            "scores": self.scores,
            "team_1_round_score": self.team_1_round_score,
            "team_2_round_score": self.team_2_round_score,
            "team_1_score": self.team_1_score,
            "team_2_score": self.team_2_score,
        }

    def deserialise(self, serialisation):
        self.players = [
            Player(serialisation=serialisation)
            for serialisation in serialisation["players"]
        ]
        self.scores = serialisation["scores"]
        self.team_1_round_score = serialisation["team_1_round_score"]
        self.team_2_round_score = serialisation["team_2_round_score"]
        self.team_1_score = serialisation["team_1_score"]
        self.team_2_score = serialisation["team_2_score"]


class DominosRound:
    def __init__(self, dominos_game=None, serialisation=None):
        if serialisation is not None:
            self.deserialise(serialisation)
        else:
            self.dominos_game = dominos_game
            self.table = []
            self.domino_coordinates = [[1650, 880]]
            self.drop_zone_coordinates = [[1540, 880], [1760, 880]]
            self.domino_orientation = [1]
            self.domino_axis_of_travel = [0]
            self.active = True
            self.player_could_not_go_count = 0
            self.do_not_switch_players = (
                False  # Used when player tried to pass but could play.
            )
            self.players = dominos_game.players
            self.current_player_idx = []

    def deal_dominos(self):
        random.shuffle(dominos_full_set)
        self.players[0].hand = dominos_full_set[0:7]
        self.players[1].hand = dominos_full_set[7:14]
        self.players[2].hand = dominos_full_set[14:21]
        self.players[3].hand = dominos_full_set[21:28]
        self.first_play = True
        indexes = [0, 1, 2, 3]
        # First player is player with double 6:
        self.current_player_idx = list(
            compress(indexes, [[6, 6] in self.players[idx].hand for idx in indexes])
        )[0]
        # TODO: If player has more than 4 of one suit then reshuffle

    def check_if_any_players_can_play(self):
        x = [
            [
                (self.table[0][0] in values) or (self.table[-1][1] in values)
                for values in player.hand
            ]
            for player in self.players
        ]
        self.one_of_players_can_play = any([any(vals) for vals in x])

    def play(
        self,
        print_output=True,
        is_simulation=False,
        input_domino_idx=None,
        end_to_play=None,
    ):
        if self.first_play:
            for player in self.players:
                if [6, 6] in player.hand:
                    # TODO: make this more pythonic. current_player_idx makes referencing cumbersome later on.
                    self.current_player_idx = [
                        player.name for player in self.players
                    ].index(player.name)
                    print("Starting player is " + player.name)
                    play = player.hand.pop(player.hand.index([6, 6]))
                    self.table = self.table + [play]
                    # TODO: The orientation should be set here, rather than when the class is created.
                    self.first_play = False
                    self.player_with_double_6 = None
                    self.print_table()
                    self.next_player()
                else:
                    ValueError(
                        "One of the players should have [6,6], but none was found"
                    )
        else:
            # Way of checking if any players can go. Could be simplified.
            self.check_if_any_players_can_play()
            if self.one_of_players_can_play:
                self = self.players[self.current_player_idx].play(
                    self,
                    print_output=print_output,
                    is_simulation=is_simulation,
                    input_domino_idx=input_domino_idx,
                    end_to_play=end_to_play,
                )
                if self.do_not_switch_players == True:
                    pass
                else:
                    self.next_player()
                self.do_not_switch_players = False

            self.check_if_any_players_can_play()
            if self.one_of_players_can_play:
                pass
            else:
                self.get_scores()
                self.active = False

    def next_player(self):
        self.current_player_idx = (self.current_player_idx + 1) % 4

    def print_table(self):
        None
        print("Table:\n------")
        print(self.table)
        print("\nHands:\n------")
        for player in self.players:
            print(player.hand)

    def get_scores(self):
        for player in self.players:
            player_score = 0
            for list in player.hand:
                for item in list:
                    player_score = player_score + item
            player.score = player_score
        self.dominos_game.team_1_round_score = (
            self.players[0].score + self.players[2].score
        )
        self.dominos_game.team_2_round_score = (
            self.players[1].score + self.players[3].score
        )
        self.dominos_game.team_1_score = (
            self.dominos_game.team_1_score + self.dominos_game.team_1_round_score
        )
        self.dominos_game.team_2_score = (
            self.dominos_game.team_2_score + self.dominos_game.team_2_round_score
        )

    # TODO: Serialise and deserialise are no longer needed since the database is used.
    def serialise(self):
        return {
            "dominos_game": self.dominos_game.serialise(),
            "table": self.table,
            "active": self.active,
            "player_could_not_go_count": self.player_could_not_go_count,
            "players": [player.serialise() for player in self.players],
            "current_player_idx": self.current_player_idx,
            "first_play": self.first_play,
            "player_could_not_go_count": self.player_could_not_go_count,
        }

    def deserialise(self, serialisation):
        self.dominos_game = DominosGame(serialisation=serialisation["dominos_game"])
        self.table = serialisation["table"]
        self.active = serialisation["active"]
        self.player_could_not_go_count = serialisation["player_could_not_go_count"]
        self.players = [
            Player(serialisation=serialisation)
            for serialisation in serialisation["players"]
        ]
        self.current_player_idx = serialisation["current_player_idx"]
        self.first_play = serialisation["first_play"]
        self.player_could_not_go_count = serialisation["player_could_not_go_count"]


class Player:
    def __init__(self, name=None, serialisation=None):
        if serialisation is not None:
            self.deserialise(serialisation)
        else:
            self.name = name
            self.score = 0
            self.hand = []

    def check_options():
        print("checking play options")

    def play(
        self,
        dominos_round,
        print_output=True,
        is_simulation=False,
        input_domino_idx=None,
        end_to_play=None,
    ):
        # TODO: These ends characteristics can be done in a neater way:
        ends = [dominos_round.table[0][0], dominos_round.table[-1][-1]]
        coordinates = dominos_round.domino_coordinates

        dominos_round.player_could_not_go_count = 0

        if input_domino_idx is not None:
            zone_to_end_name = {"zoneA": "left", "zoneB": "right"}
            zone_to_end_number = {"zoneA": 0, "zoneB": 1}
            selected_domino = [
                self.hand[input_domino_idx],
                [
                    self.hand[input_domino_idx].index(
                        ends[zone_to_end_number[end_to_play]]
                    ),
                    zone_to_end_name[end_to_play],
                ],
            ]
        else:
            # Computer selects domino to play:
            ##################################
            # # Idea for mcts:
            # Set up so only team 1 uses mcts_selection so if its good they should always win:
            # if not is_simulation:
            #     if [player.name for player in dominos_round.players].index(dominos_round.current_player.name) in [0,2]:
            #         if len(options) > 1:
            #             selected_domino = mcts_selection(options, dominos_round, game)
            #         else:
            #             selected_domino = options[0]
            #     else:
            #         selected_domino = options[0]

            options = []
            for a_domino in self.hand:
                if ends[0] in a_domino:
                    options = options + [
                        [a_domino] + [[a_domino.index(ends[0]), "left"]]
                    ]
                elif ends[1] in a_domino:
                    options = options + [
                        [a_domino] + [[a_domino.index(ends[1]), "right"]]
                    ]
            if options:
                if self.name.__contains__("computer"):
                    random.shuffle(options)
                    selected_domino = options[0]
                else:
                    print("Player has options so cannot pass")
                    dominos_round.do_not_switch_players = True
                    return dominos_round
            else:
                if print_output:
                    print(
                        dominos_round.players[dominos_round.current_player_idx].name
                        + " could not go\n*********************"
                    )
                dominos_round.player_could_not_go_count = (
                    dominos_round.player_could_not_go_count + 1
                )
                return dominos_round

        ##################################

        selected_domino_idx = dominos_round.players[
            dominos_round.current_player_idx
        ].hand.index(selected_domino[0])
        domino_idx = selected_domino[1][0]
        table_idx = selected_domino[1][1]
        play = dominos_round.players[dominos_round.current_player_idx].hand.pop(
            selected_domino_idx
        )

        if table_idx == "left":
            end_idx = 0
            end_travel_direction = -1
        else:
            end_idx = -1
            end_travel_direction = 1

        neighbouring_domino = {
            "value": dominos_round.table[end_idx],
            "orientation": dominos_round.domino_orientation[end_idx],
            "axis_of_travel": dominos_round.domino_axis_of_travel[end_idx],
        }

        direction_axes = np.matrix(
            "1, 0; 0, 1; -1 0; 0 -1"
        )  # Four travel directions: ->, V, <-, ^

        def position_change(
            neighbouring_domino, domino_to_be_played, turn=0, drop_zone=False
        ):
            # Create named tuple for movement variable
            #  - horizontal/vertical, speed, and +/- (-ve = left and down, +ve = right and up)
            distances_dict = {
                "continue": 140,
                "orientation_change": 110,
                "turn": 35,
            }
            current_axis = neighbouring_domino["axis_of_travel"]
            print("Domino to be played is: " + str(domino_to_be_played))
            if (not drop_zone) and (
                (domino_to_be_played[0][0] == domino_to_be_played[0][1])
                or (neighbouring_domino["value"][0] == neighbouring_domino["value"][1])
            ):
                distance = distances_dict["orientation_change"]
                axis = current_axis
                if domino_to_be_played[0][0] == domino_to_be_played[0][1]:
                    orientation = (neighbouring_domino["orientation"] + 1) % 4
                    move = (
                        direction_axes[neighbouring_domino["orientation"]]
                        * end_travel_direction
                    )
                    position_change = move * distance
                elif neighbouring_domino["value"][0] == neighbouring_domino["value"][1]:
                    orientation = (neighbouring_domino["orientation"] - 1) % 4
                    move = direction_axes[orientation] * end_travel_direction
                    position_change = move * distance
            elif turn != 0:
                distance = distances_dict["orientation_change"]
                orientation = (neighbouring_domino["orientation"] + turn) % 4
                axis = int(np.abs(current_axis - 1))
                move = (
                    direction_axes[neighbouring_domino["orientation"]]
                    * end_travel_direction
                )
                position_change = move * distance + (
                    direction_axes[orientation]
                    * (end_travel_direction * distances_dict["turn"])
                )
            else:
                distance = distances_dict["continue"]
                orientation = neighbouring_domino["orientation"]
                axis = current_axis
                move = (
                    direction_axes[neighbouring_domino["orientation"]]
                    * end_travel_direction
                )
                position_change = move * distance

            return position_change, orientation, axis

        turn = 0
        pos_change, orientation, axis = position_change(
            neighbouring_domino, selected_domino, turn
        )

        play_coordinates = coordinates[end_idx] + pos_change

        boundaries = [
            [1000, 2300],
            [420, 1180],
        ]
        # The locations of the domino train also need to be considered, to stop crossing-over.
        # Also not sure if this method handles the cornering.
        # TODO: The method used below for obtaining 0, 1 from -1, 1 needs to be improved
        # int((end_travel_direction + 1) / 2)
        if (
            end_travel_direction
            * (
                boundaries[orientation % 2][int((end_travel_direction + 1) / 2)]
                - play_coordinates.tolist()[0][orientation % 2]
            )
            < 0
        ):
            turn = 1  # Here the available space on the board needs to be checked and the area with most space are moved into.
            pos_change, orientation, axis = position_change(
                neighbouring_domino,
                selected_domino,
                turn,
            )
            play_coordinates = coordinates[end_idx] + pos_change

        # Drop zone coordinates:
        domino_to_play = {
            "value": play,
            "orientation": orientation,
            "axis_of_travel": axis,
        }
        # Ideally multiple potential coordinates should be calculated, then one will be selected in phaser depending on the domino selected.
        # Double, corners, or continue. Then the players could choose the direction of play.
        pos_change, _, _ = position_change(
            domino_to_be_played=None,
            neighbouring_domino=domino_to_play,
            drop_zone="true",
        )
        drop_zone_coordinates = play_coordinates + pos_change

        play_coordinates = play_coordinates.tolist()[0]
        drop_zone_coordinates = drop_zone_coordinates.tolist()[0]

        if table_idx == "left":
            if domino_idx == 0:
                play.reverse()
            dominos_round.table = [play] + dominos_round.table

            dominos_round.domino_axis_of_travel = [
                axis
            ] + dominos_round.domino_axis_of_travel

            dominos_round.domino_coordinates = [
                play_coordinates
            ] + dominos_round.domino_coordinates

            dominos_round.domino_orientation = [
                orientation
            ] + dominos_round.domino_orientation

            dominos_round.drop_zone_coordinates = [
                drop_zone_coordinates
            ] + dominos_round.drop_zone_coordinates

        elif table_idx == "right":
            if domino_idx == 1:
                play.reverse()
            dominos_round.table = dominos_round.table + [play]

            dominos_round.domino_axis_of_travel = (
                dominos_round.domino_axis_of_travel + [axis]
            )

            dominos_round.domino_coordinates = dominos_round.domino_coordinates + [
                play_coordinates
            ]

            dominos_round.domino_orientation = dominos_round.domino_orientation + [
                orientation
            ]

            dominos_round.drop_zone_coordinates = (
                dominos_round.drop_zone_coordinates + [drop_zone_coordinates]
            )

        if print_output:
            print(
                dominos_round.players[dominos_round.current_player_idx].name
                + " has played"
                + "\n"
            )
            print("Table:\n------")
            print(dominos_round.table)
            print("\nHands:\n------")
            for player in dominos_round.players:
                print(player.hand)
            print("\n")

        return dominos_round

    def serialise(self):
        return {"name": self.name, "score": self.score, "hand": self.hand}

    def deserialise(self, serialisation):
        self.name = serialisation["name"]
        self.score = serialisation["score"]
        self.hand = serialisation["hand"]


if __name__ == "__main__":

    scores = [0, 0]

    for n in range(0, 100):
        # Initiate game:
        ################
        game = DominosGame(player_names=["player1", "player2", "player3", "player4"])

        # Start playing:
        ################
        while True:
            # New round:
            dominos_round = DominosRound(game)
            dominos_round.deal_dominos()

            while dominos_round.active:
                dominos_round.play(print_output=False)

            game = dominos_round.dominos_game
            # game.print_round_summary()

            if (game.team_1_score > 100) or (game.team_2_score > 100):
                break

        game.print_final_scores()
        if game.team_1_score < game.team_2_score:
            scores[0] += 1
        elif game.team_2_score < game.team_1_score:
            scores[1] += 1

    print(scores)
    print("finish")
