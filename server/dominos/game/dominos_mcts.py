import copy
import random
import numpy as np
import time

# Known are:
# - round.current_player.hand
# - round.table

dominos_full_set = [
    [0,0],
    [1,0], [1,1],
    [2,0], [2,1], [2,2],
    [3,0], [3,1], [3,2], [3,3],
    [4,0], [4,1], [4,2], [4,3], [4,4],
    [5,0], [5,1], [5,2], [5,3], [5,4], [5,5],
    [6,0], [6,1], [6,2], [6,3], [6,4], [6,5], [6,6]
    ]


def mcts_selection(options, round, game):
    n_simulations = 100
    options_win_tally = np.zeros(len(options))
    current_team = [player.name for player in round.players].index(round.current_player.name) % 2

    t = time.time()
    print('starting_simulations')

    for sim in range(0, n_simulations):
        unseen_dominos = [
            [0,0],
            [1,0], [1,1],
            [2,0], [2,1], [2,2],
            [3,0], [3,1], [3,2], [3,3],
            [4,0], [4,1], [4,2], [4,3], [4,4],
            [5,0], [5,1], [5,2], [5,3], [5,4], [5,5],
            [6,0], [6,1], [6,2], [6,3], [6,4], [6,5], [6,6]
            ]
        for domino in round.table:
            domino.sort(reverse=True)
            unseen_dominos.pop(unseen_dominos.index(domino))
        for domino in round.current_player.hand:
            domino.sort(reverse=True)
            unseen_dominos.pop(unseen_dominos.index(domino))
        random.shuffle(unseen_dominos)
        
        simulation_game = copy.deepcopy(game)
        simulation_round = copy.deepcopy(round)

        idx = [0,1,2,3]
        idx.pop([player.name for player in simulation_round.players].index(simulation_round.current_player.name))
        for index in idx:
            N = len(simulation_round.players[index].hand)
            simulated_hand = []
            for n in range(0, N):
                simulated_hand = simulated_hand + [unseen_dominos.pop()]
            simulation_round.players[index].hand = simulated_hand
        
        selected_option = np.random.choice([n for n in range(len(options))])
        selected_domino = options[selected_option]
        
        selected_domino_idx = simulation_round.current_player.hand.index(selected_domino[0])
        domino_idx = selected_domino[1][0]
        table_idx = selected_domino[1][1]
        play = simulation_round.current_player.hand.pop(selected_domino_idx)

        if table_idx == 'left':
            if domino_idx == 0:
                play.reverse()
            table = [play] + simulation_round.table
        elif table_idx == 'right':
            if domino_idx == 1:
                play.reverse()
            table = simulation_round.table + [play]
        
        simulation_round.next_player()

        # Start playing:
        ################
        while simulation_round.active:
            simulation_round.play(print_output=False, is_simulation=True)

        simulation_game = simulation_round.dominos_game

        scores = [simulation_game.team_1_round_score, simulation_game.team_2_round_score]
        if (scores.index(min(scores)) == current_team):
            options_win_tally[selected_option] = options_win_tally[selected_option] + 1
        else:
            options_win_tally[selected_option] = options_win_tally[selected_option] + 1
    
    final_choice = list(options_win_tally).index(max(options_win_tally))
    print('win probabilities = ' + str(options_win_tally/n_simulations))
    print('simulation time = ' + str(time.time() - t))

    selected_domino = options[final_choice]

    return selected_domino



if __name__=="__main__":
    print('finish')
