import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import io from "socket.io-client";
import VueSocketIO from "vue-socket.io";

Vue.use(new VueSocketIO({
  debug: false,
  connection: io("http://localhost:5000/"), //`//${process.env.VUE_APP_FLASK_HOST}:5000`) //`//${window.location.host}`)
  vuex: {
    store,
    actionPrefix: 'WS_',
    mutationPrefix: 'WS_'
  }
}));

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 0,
    pong: 0,
    table: [[6, 1]],
    playerName: "not_set",
    gameInactive: null,
    roomId: null
  },
  mutations: {
    increment(state) {
      state.count++
    },
    pong_increment(state) {
      state.pong++
    },
    storeTable(state, table) {
      state.table = table;
    },
    storePlayerName(state, name) {
      state.playerName = name;
    },
    storeRoomId(state, room) {
      state.roomId = room;
    },
    makeGameActive(state) {
      state.gameInactive = false;
    },
  },
})

new Vue({
  store: store,
  render: h => h(App),
}).$mount('#app')
