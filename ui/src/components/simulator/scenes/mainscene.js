import { Scene } from 'phaser';
import UIHandler from '../helpers/UIHandler';
import InteractivityHandler from '../helpers/InteractivityHandler';
import SocketHandler from '../helpers/SocketHandler';
import ra_dominos from '@/components/simulator/scenes/spritesheet.png'
export default class MainScene extends Scene {
    constructor(room, name) {
        super({ key: 'MainScene' });
        this.room = room
        this.name = name
    }
    preload() {
        this.load.spritesheet('dominos', ra_dominos, { frameWidth: 70, frameHeight: 71 });
    }

    create() {
        // this.SocketHandler = new SocketHandler(this, this.roomId);
        // var s = this.add.sprite(80, 380, 'einstein');
        // s.rotation = 0.14;
        this.playerCards = [];
        this.opponentCards = [];

        this.cardsInPlayerHand = 0;
        this.cardsInOpponentHand = 0;

        this.currentPlayer = 0;
        this.zoneA_accepts = 6;
        this.zone = null;
        this.zone2 = null;
        this.table = null;

        var page_width = 3000
        var page_height = 1600

        this.UIHandler = new UIHandler(this, page_width, page_height);
        this.InteractivityHandler = new InteractivityHandler(this, page_width, page_height);
        this.SocketHandler = new SocketHandler(this, page_width, page_height);

    }
}