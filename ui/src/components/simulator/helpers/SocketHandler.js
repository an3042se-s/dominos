import { io } from "socket.io-client";
import Domino from "./domino";

export default class SocketHandler {
    constructor(scene, page_width, page_height) {
        scene.socket = io("http://localhost:5000/");
        scene.socket.on('connect', () => {
            console.log('Phaser connected!');
            scene.socket.emit('join', { room: scene.room, player_name: scene.name });
        })
        scene.socket.on("update_game", function (msg) {
            scene.socket.emit("game_updated", { msg })
        })
        scene.socket.on("update_player_hand", function (msg) {
            for (let card in scene.playerCards) {
                scene.playerCards[card].destroy();
            }
            var table = msg.table
            var current_player = msg.current_player
            scene.currentPlayer = current_player
            var domino_coordinates = msg.domino_coordinates
            var domino_orientation = msg.domino_orientation
            var drop_zone_coordinates = msg.drop_zone_coordinates

            for (var itable = 0; itable < table.length; itable++) {
                var x2table = domino_coordinates[itable][0]
                var y2table = domino_coordinates[itable][1];
                let newDomino = new Domino(scene);
                let val = newDomino.render(x2table, y2table, [table[itable][0], table[itable][1]], domino_orientation[itable], 1)
                val.input.enabled = false;
                scene.playerCards.push(val);
            }

            // Bottom
            var player = msg.player
            for (var i2 = 0; i2 < player.length; i2++) {
                var x2 = 0.5 * page_width + i2 * 70;
                var y2 = page_height;
                let newDomino = new Domino(scene);
                scene.playerCards.push(newDomino.render(x2, y2, [player[i2][0], player[i2][1]], 1, "0"));
            }

            // Left
            var player1 = msg.player1
            for (var i3 = 0; i3 < player1.length; i3++) {
                var x3 = 0.1 * page_width;
                var y3 = 0.45 * page_height + i3 * 70;
                let newDomino = new Domino(scene);
                scene.playerCards.push(newDomino.render(x3, y3, [player1[i3][0], player1[i3][1]], 0, 1));
            }

            // Top
            var player2 = msg.player2
            for (var i = 0; i < player2.length; i++) {
                var x = 0.5 * page_width + i * 70;
                var y = 0.1 * page_height;
                let newDomino = new Domino(scene);
                scene.playerCards.push(newDomino.render(x, y, [player2[i][0], player2[i][1]], 1, 2));
            }

            // Right
            var player3 = msg.player3
            for (var i4 = 0; i4 < player3.length; i4++) {
                var x4 = page_width;
                var y4 = 0.45 * page_height + i4 * 70;
                let newDomino = new Domino(scene);
                scene.playerCards.push(newDomino.render(x4, y4, [player3[i4][0], player3[i4][1]], 0, 3));
            }

            var zone = scene.add.zone(drop_zone_coordinates[0][0], drop_zone_coordinates[0][1], 140, 70).setRectangleDropZone(140, 70);
            zone.name = "zoneA"
            zone.setData({ "accepts": msg.zoneA_accepts, "bind_point": 1 })
            scene.zone = zone

            var zone2 = scene.add.zone(drop_zone_coordinates[drop_zone_coordinates.length - 1][0], drop_zone_coordinates[drop_zone_coordinates.length - 1][1], 140, 70).setRectangleDropZone(140, 70);
            zone2.name = "zoneB"
            zone2.setData({ "accepts": msg.zoneB_accepts, "bind_point": 0 })
            scene.zone2 = zone2

            scene.team_1_score.destroy();
            scene.team_2_score.destroy();

            scene.team_1_score = scene.add.text(page_width, 0.8 * page_height, "Team 1 score: " + msg.team_1_score).setFontSize(50).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
            scene.team_2_score = scene.add.text(page_width, 0.9 * page_height, "Team 2 score: " + msg.team_2_score).setFontSize(50).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
        })
    }
}