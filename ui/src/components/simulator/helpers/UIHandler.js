export default class UIHandler {

    constructor(scene, page_width, page_height) {

        var area_width = 500
        var area_height = 100

        //Upper
        scene.opponentHandArea = scene.add.rectangle(0.55 * page_width, 0.1 * page_height, area_width, area_height);
        scene.opponentHandArea.setStrokeStyle(4, 0x17a2b8);

        //Left
        scene.player2HandArea = scene.add.rectangle(0.1 * page_width, 0.55 * page_height, area_height, area_width);
        scene.player2HandArea.setStrokeStyle(4, 0x17a2b8);

        //Right
        scene.player3HandArea = scene.add.rectangle(page_width, 0.55 * page_height, area_height, area_width);
        scene.player3HandArea.setStrokeStyle(4, 0x17a2b8);

        //Lower
        scene.playerHandArea = scene.add.rectangle(0.55 * page_width, page_height, area_width, area_height);
        scene.playerHandArea.setStrokeStyle(4, 0x17a2b8);

        scene.startNewGame = scene.add.text(page_width, 0.1 * page_height, "Start New Game").setFontSize(40).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
        scene.room_name = scene.add.text(0, 0, "Room Name: " + scene.room).setFontSize(40).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
        scene.pass = scene.add.text(0.35 * page_width, page_height, "Pass").setFontSize(40).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
        scene.continuePlay = scene.add.text(0.73 * page_width, page_height, "Next Round").setFontSize(40).setFontFamily('Arial').setColor('#17a2b8').setInteractive();

        scene.startNewGame.on('pointerdown', () => {
            scene.socket.emit("start_game", { player_name: "test", number_of_computer_players: 3 });
        })
        scene.pass.on('pointerdown', () => {
            scene.socket.emit("play_domino", { "domino_to_play": [], "end_to_play": [], "page_width": page_width, "page_height": page_height },)
        })
        scene.continuePlay.on('pointerdown', () => {
            scene.socket.emit("continue_play", {},)
        })

        scene.tableArea = scene.add.rectangle(0.55 * page_width, 0.55 * page_height, 0.8 * page_width, 0.8 * page_height);
        scene.tableArea.setStrokeStyle(4, 0x17a2b8);

        scene.team_1_score = scene.add.text(page_width, 0.8 * page_height, "Team 1 score: " + 0).setFontSize(50).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
        scene.team_2_score = scene.add.text(page_width, 0.9 * page_height, "Team 2 score: " + 0).setFontSize(50).setFontFamily('Arial').setColor('#17a2b8').setInteractive();
    }
}