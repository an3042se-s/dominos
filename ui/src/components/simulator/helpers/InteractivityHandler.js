export default class InteractivityHandler {
    constructor(scene, page_width, page_height) {

        scene.input.on('pointerout', (event, gameObjects) => {
            if (gameObjects[0].type === "Container") {
                if (scene.cardPreview !== undefined) {
                    scene.cardPreview.destroy();
                }
            }
        });

        scene.startNewGame.on('pointerover', () => {
            scene.startNewGame.setColor('#ffffff');
        })
        scene.startNewGame.on('pointerout', () => {
            scene.startNewGame.setColor('#17a2b8');
        })
        scene.pass.on('pointerover', () => {
            scene.pass.setColor('#ffffff');
        })
        scene.pass.on('pointerout', () => {
            scene.pass.setColor('#17a2b8');
        })
        scene.continuePlay.on('pointerover', () => {
            scene.continuePlay.setColor('#ffffff');
        })
        scene.continuePlay.on('pointerout', () => {
            scene.continuePlay.setColor('#17a2b8');
        })

        //  Just a visual display of the drop zone
        var graphics = scene.add.graphics();

        scene.input.on('dragstart', function (pointer, gameObject) {

            scene.children.bringToTop(gameObject);
            graphics.lineStyle(2, 0xffff00);
            graphics.strokeRect(scene.zone.x - scene.zone.input.hitArea.width / 2, scene.zone.y - scene.zone.input.hitArea.height / 2, scene.zone.input.hitArea.width, scene.zone.input.hitArea.height);
            graphics.strokeRect(scene.zone2.x - scene.zone2.input.hitArea.width / 2, scene.zone2.y - scene.zone2.input.hitArea.height / 2, scene.zone2.input.hitArea.width, scene.zone2.input.hitArea.height);

        }, scene);

        scene.input.on('drag', function (pointer, gameObject, dragX, dragY) {

            gameObject.x = dragX;
            gameObject.y = dragY;

        });

        scene.input.on('dragenter', function (pointer, gameObject, dropZone) {
            var text = gameObject.data.values.domino //[gameObject.data.values.endA.toString(), gameObject.data.values.endB.toString()]
            if ((text.includes(dropZone.data.values.accepts))) {
                graphics.lineStyle(2, 0x00ff11);
                graphics.strokeRect(dropZone.x - dropZone.input.hitArea.width / 2, dropZone.y - dropZone.input.hitArea.height / 2, dropZone.input.hitArea.width, dropZone.input.hitArea.height);
            } else {
                graphics.lineStyle(2, 0xfb1239);
                graphics.strokeRect(dropZone.x - dropZone.input.hitArea.width / 2, dropZone.y - dropZone.input.hitArea.height / 2, dropZone.input.hitArea.width, dropZone.input.hitArea.height);
            }
        });

        scene.input.on('dragleave', function (pointer, gameObject, dropZone) {
            graphics.lineStyle(2, 0xffff00);
            graphics.strokeRect(dropZone.x - dropZone.input.hitArea.width / 2, dropZone.y - dropZone.input.hitArea.height / 2, dropZone.input.hitArea.width, dropZone.input.hitArea.height);
        });

        scene.input.on('drop', function (pointer, gameObject, dropZone) {
            // var text = gameObject.data.values.domino
            if (gameObject.data.values.domino.includes(dropZone.data.values.accepts)) {
                if (gameObject.data.values.player == "0") {
                    gameObject.x = dropZone.x;
                    gameObject.y = dropZone.y;

                    var idx = gameObject.data.values.domino.indexOf(dropZone.data.values.accepts)
                    // If the two domino values are the same, the domino should be dropped at a right angle
                    var rotation = (idx - dropZone.data.values.bind_point) * Math.PI
                    gameObject.setRotation(rotation);
                    scene.socket.emit("play_domino", { "domino_to_play": gameObject.data.values.domino, "end_to_play": dropZone.name, "page_width": page_width, "page_height": page_height },)
                    dropZone.setData({ "accepts": gameObject.data.values.domino[0] });
                    gameObject.input.enabled = false;
                    graphics.clear()
                } else {
                    gameObject.x = gameObject.input.dragStartX;
                    gameObject.y = gameObject.input.dragStartY;

                    graphics.clear()
                }

            } else {
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;

                graphics.clear()
            }

        });

        scene.input.on('dragend', function (pointer, gameObject, dropped) {

            if (!dropped) {
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;

                graphics.clear()
            }

        });

    }
}