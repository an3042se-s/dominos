export default class Domino {
    constructor(scene) {
        // let card;
        this.render = (x, y, domino, direction, player) => {
            let sideA = scene.add.sprite(-35, 0, 'dominos', domino[0]);
            let sideB = scene.add.sprite(35, 0, 'dominos', domino[1]);
            sideB.rotation = Math.PI
            this.card = scene.add.container(x, y, [sideA, sideB]).setSize(sideA.width + sideB.height, sideA.height).setData({ "domino": domino }).setInteractive();
            this.card.setData({ "player": player })
            //"right"
            if (direction == 0) {
                this.card.setData({ "rotation": [Math.PI, 0] })
            }
            //"down"
            else if (direction == 1) {
                this.card.setRotation(Math.PI / 2)
                this.card.setData({ "rotation": [- (Math.PI / 2), (Math.PI / 2)] })
            }
            //"left"
            else if (direction == 2) {
                this.card.setRotation(Math.PI)
                this.card.setData({ "rotation": [0, Math.PI] })
            }
            //"up"
            else if (direction == 3) {
                this.card.setRotation(-Math.PI / 2)
                this.card.setData({ "rotation": [(Math.PI / 2), - (Math.PI / 2)] })
            }
            scene.input.setDraggable(this.card);
            return this.card;
        }
        this.destroy = () => {
            this.card.destroy();
        }
    }
}

// export default class Domino {
//     constructor(scene) {
//         // let card;
//         this.render = () => {
//             let cardBG = scene.add.rectangle(0, 0, 140, 70, 0x17a2b8);
//             cardBG.setStrokeStyle(4, 0xffffff);
//             this.card = scene.add.container(900, 520, cardBG).setInteractive();
//             // var c = this.add.sprite(900, 520, 'dominos', 1);
//             // var d = this.add.sprite(970, 520, 'dominos', 2);
//             // d.rotation = 3.14
//             // this.card.add.sprite(c)
//             // this.card.add.sprite(d)
//             scene.input.setDraggable(this.card);
//             return this.card;
//         }
//         this.destroy = () => {
//             this.card.destroy();
//         }
//     }
// }