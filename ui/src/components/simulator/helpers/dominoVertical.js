export default class DominoVertical {
    constructor(scene) {
        // let card;
        this.render = (x, y, domino) => {
            let sideA = scene.add.sprite(0, -35, 'dominos', domino[0]);
            let sideB = scene.add.sprite(0, 35, 'dominos', domino[1]);
            sideA.rotation = 3.14 * 0.5
            sideB.rotation = 3.14 * 1.5
            this.card = scene.add.container(x, y, [sideA, sideB]).setSize(sideA.width + sideB.height, sideA.height).setData({ "domino": domino }).setInteractive();
            scene.input.setDraggable(this.card);
            return this.card;
        }
        this.destroy = () => {
            this.card.destroy();
        }
    }
}

// export default class Domino {
//     constructor(scene) {
//         // let card;
//         this.render = () => {
//             let cardBG = scene.add.rectangle(0, 0, 140, 70, 0x17a2b8);
//             cardBG.setStrokeStyle(4, 0xffffff);
//             this.card = scene.add.container(900, 520, cardBG).setInteractive();
//             // var c = this.add.sprite(900, 520, 'dominos', 1);
//             // var d = this.add.sprite(970, 520, 'dominos', 2);
//             // d.rotation = 3.14
//             // this.card.add.sprite(c)
//             // this.card.add.sprite(d)
//             scene.input.setDraggable(this.card);
//             return this.card;
//         }
//         this.destroy = () => {
//             this.card.destroy();
//         }
//     }
// }