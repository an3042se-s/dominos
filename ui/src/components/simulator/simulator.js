import Phaser from 'phaser';
import MainScene from './scenes/mainscene';

function launch(room, name) {

    let main_scene = new MainScene(room, name);

    let config = {
        type: Phaser.AUTO,
        scale: {
            mode: Phaser.Scale.FIT,
            width: 3500,
            height: 1800
        },
        scene: [main_scene]
    };

    let game = new Phaser.Game(config);

    return game;
}

export default launch;
export { launch };
